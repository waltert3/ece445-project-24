- Worked on PCB Design
- Using design and relay mentioned by Jason, I added relays and A diode switching circuit to the schematic
- I then implemented the diode and relay circuit onto the PCB
- Our Micro USB connector part had female headers so changed the PCB headers to male in order to connect it
- Uploaded final PCB to our lab notebook

- Met with Jeff at machine shop in order to confirm configurations for machined parts