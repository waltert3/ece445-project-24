## Sept 18 2023

### Received feedback on Project Proposal. We worked on fixing the proposal based on comments. 
- Changed Problem Statement to only focus on problem at ahnd without mentioning any solutions
- Changed Solution to be more broad and to specify the issue at hand
- Changed Block Diagram
    - Diagram includes descriptions of each action
    - Diagram includues more specific components, such as power converters and wifi protocol
    - Diagram is more clear on what is being controlled. The ESP8266 controls the relay which controls the lamp
- Changed Subsystem Requirements to include different details than the High Level Requirements
- Changed Block Diagram to have different colored arrows in order to describe different actions
