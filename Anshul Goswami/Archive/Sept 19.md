## Meeting with TA on feedback regarding proposal. Here is what we talked about.
- The proposal will be graded by the writing center for final grade
- Expand on the subsystem requirements. Add more details. 
- Regarding Tolerance Analysis
    - Discuss about how much decibals the microphone can pick up
- Add another subsystem requirement. How we will actually pick up voice commands using software
- Accuracy, F1, Kappa (Reliability Metric) in tolerance Analysis
