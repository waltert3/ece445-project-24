## Oct 17 2023
- Remade the PCB design schematic and PCB to make it more organized and with fewer wires and connections
- Submitted PCB to Audit to TA. TA said that hole sizes were too small so modified ESP-32 Footprint
- Did another audit and submitted to TA, which passed and order was sent
