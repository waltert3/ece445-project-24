# Anshul Goswami

[[_TOC_]]

# 2023-09-14

Today, I worked on the project proposal with my team. This is the initial stage so we have a lot of uncertainly about where we will be heading with the project, but luckily we will be receiving feedback. Luckily, I already have a good idea of the circuit, since I have done some research on how smart switch circuits work.

# 2023-09-18

Received feedback on Project Proposal. We worked on fixing the proposal based on comments. 
- Changed Problem Statement to only focus on problem at ahnd without mentioning any solutions
- Changed Solution to be more broad and to specify the issue at hand
- Changed Block Diagram
    - Diagram includes descriptions of each action
    - Diagram includues more specific components, such as power converters and wifi protocol
    - Diagram is more clear on what is being controlled. The ESP8266 controls the relay which controls the lamp
- Changed Subsystem Requirements to include different details than the High Level Requirements
- Changed Block Diagram to have different colored arrows in order to describe different actions

![](block_diagram.png)

# 2023-09-19
Meeting with TA on feedback regarding proposal. Here is what we talked about.
- The proposal will be graded by the writing center for final grade
- Expand on the subsystem requirements. Add more details. 
- Regarding Tolerance Analysis
    - Discuss about how much decibals the microphone can pick up
- Add another subsystem requirement. How we will actually pick up voice commands using software
- Accuracy, F1, Kappa (Reliability Metric) in tolerance Analysis

# 2023-09-27
We spoke with the machine shop for the initial design of our lamp box and the raspberri pi box. The raspberri
pi box is simple. It is just an enclosure the size of the Raspberri Pi that has a hole for the microphone wire. However,
we will need to provide dimensions of our PCB and other details such as where the screw terminals are located for the 3 outlet 
wires, so that the machine shop would know where to put holes in the enclosure

# 2023-10-01
- Prepared for Design Presentation by reviewing
- design doc

# 2023-10-06
- Worked on PCB at Board Review

# 2023-10-08

Today, I worked on creating the programmer circuit myself. I based the design on the ECE 445 website. Below is the example circuit.

![](ece445_circuit.png)

Below is the circuit I created

![](my_circuit.png)

# 2023-10-13
Machine shop wanted a clearer version of our outlet box design. Created a cleaner image using google drawings. Machine shop liked the design and approved it
![](lamp_box.png)

# 2023-10-17
- Remade the PCB design schematic and PCB to make it more organized and with fewer wires and connections
- Submitted PCB to Audit to TA. TA said that hole sizes were too small so modified ESP-32 Footprint
- Did another audit and submitted to TA, which passed and order was sent

![](audit.png)

# 2023-10-20
- Spoke with Jason regarding how to decide what relay to use
- Speaking with Jason, 3.3V relays are not common to buy. We instead decided to use a power source of 5V to power a relay. And then the ESP32-E will output 3.3V into an NPN transistor, which uses a 5V gate source. This will power our new relay
- Took notes on the design to be implemented later

![](conversion.png)

# 2023-10-22
- Worked on PCB Design
- Using design and relay mentioned by Jason, I added relays and A diode switching circuit to the schematic
- I then implemented the diode and relay circuit onto the PCB
- Our Micro USB connector part had female headers so changed the PCB headers to male in order to connect it
- Uploaded final PCB to our lab notebook
- Met with Jeff at machine shop in order to confirm configurations for machined parts


![](relay_circuit.png)

# 2023-10-23
- Worked on Speech Recognition using the VOSK API
- https://www.youtube.com/watch?v=SqVeAxrPAB0

# 2023-10-24
- Met with TA at 9:30
- Finished redesigning PCB by myself. Submitted PCB for audit on PCB way
- Sent PCB to Jeff

# 2023-10-25
Filled out individual progress report

# 2023-10-27
Finalized specific parts we need for our voltage regular circuit

# 2023-10-30
Ordered more parts on PCB way, making sure each part matches the footprint I put on KiCad

# 2023-11-14
On this day, we started working on soldering the PCB boards. Since we had two, I worked on soldering one board while Anish did the other. We were also able to get most of the parts of the PCB when we began soldering. However, we though that the ECE supply shop would have 100 micro Farad capaciators in SMD form but they didn't. They only had electrolytic capicators. Thus, I just soldered those carefully to an SMD pad, since it will still work. You just have to make sure it fits properly. 

# 2023-11-21
During Thanksgiving Break, I spent a lot of time myself working on code that can be programmed into the ESP32-E. Since we didn't test out the PCB, I programmed everything on an ESP8266 DEV board I had lying around. I am trying to use a service called Blynk that will allow the ESP8266 to connect to Blink's cloud IOT service. That way, the Raspberri Pi, who is connected to the internet, can send a request to that API Service

- Blink Website: https://blynk.io/

![](iot.jpeg)


# 2023-11-24
I have been able to succesfully connect the ESP to Blynk API and control it using the internet. However, the library that lets the raspberri PI communicate with the ESP8266 is completly depreciated. Thus, we cannot use this method. I asked the TA on an alternative method and he suggested we can use the ESP32-E as a webserver and send GET requests to the Raspberri Pi. This looks like a perfect solution. 

![](communicate.png)

# 2023-11-27
Got the ESP32-E code working flawlessly. It works great

![](esp_code.png)

# 2023-12-4
Worked on final presentation with group. Unfortuanly, our PCB overall did not work. Thus, in the final presentation, we spent as much time as possible trying to find out the modules that do work, so that we can tell the final presentation committee on what is working on the circuit. 