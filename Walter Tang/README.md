# WALTER TANG'S LAB NOTEBOOK

[[_TOC_]]

# 9/13/2023:

Today, we worked on our project proposal. We tried to write down as much important information as possible, but we are still having a bit of trouble envisioning how our full project is going to look like. We plan on meeting tomorrow to discuss and finalize any points before the proposal is due. We also started filling out our team contract because we want to ensure that we have some guidelines set for working as a team before we continue.

# 9/14/2023

We met to continue working on our project proposal. We discussed many possible revisions to our original idea because we were worried that our proposed project was going to be too difficult and we would be unable to complete it within the given timeframe. None of us really have much experience working with PCBs so we want to give us enough time to be able to work on that thoroughly. In the end, we ended up finalizing our proposal with plans of future revisions if our TA recommends. We also completed the team contract and submitted it. I have linked our proposal [here](https://gitlab.engr.illinois.edu/waltert3/ece445-project-24/-/blob/main/ECE_445_Proposal.pdf?ref_type=heads). 
# 9/18/2023

We recieved feedback from our TA on ways we could improve our project proposal, such as coming up with better requirements. We met with our TA earlier this week to talk about possible suggestions. One thing we spent a lot of time revising was our block diagram. 

![](revised_block_diagram.png)

We had to reconfigure a lot of our connections and determine what kind of signals they were going to be. We also added numbers to show our intended voltages - but we may need to come back and revise this later. We plan on revisiting this during our design review, because we are not 100% sure of the implications just yet. 


# 9/28/2023

We spent a lot of time today working on our design document and submitted it. One thing I mainly took at was what kind of algorithms and datasets we were gonna use for voice recognition. 

[AssemblyAi](https://www.assemblyai.com/) - seemed like a good option, they are a small company that has recieved a lot of funding to develop speech models. However, their API does not seem free to use.

[Vosk](https://alphacephei.com/vosk/) - an alternative to AssemblyAi. While their performance doesn't seem to be as good as AssemblyAi, they are free to use and have been used before with some other smart appliances, which show that it could be a good fit for our project.

# 10/9/2023

Working on the Design Document and revisions. We've also started working on our PCB design - and one thing we're a little unsure about is what relay we're going to use particulary over [solid state](https://www.digikey.com/en/products/filter/solid-state-relays/183?s=N4IgjCBcoCwdIDGUBmBDANgZwKYBoQB7KAbRAGYAGAdjBkpAF0CAHAFyhAGU2AnASwB2AcxABfAgDYAnFFDJI6bPiKkQ9aeU0gCcMAA5p1Jqw6RufIaIngqcpKky4CxSGXqTJNEyHaceAiLiBGDShvYKSs6qbiAMjDYAtABMEVB8AK4qrmQArDogsgkEibIICpnZahAJNqmxvDgYaACeTGJAA) and [electromechanical relays](https://www.digikey.com/en/products/filter/power-relays-over-2-amps/188?s=N4IgjCBcoGwJxVAYygMwIYBsDOBTANCAPZQDaIALAAxwDMdIhFYYAHHAOwgC6hADgBcoIAMoCATgEsAdgHMQAX0JhqCaCBSQMOAsTKUqMGFS68Qg4WKlzFyuO0Qa0WPIRKRyVHkpABaNchQEgCuuu7kENw%2BvgBMjpohYfoArIwgCFE%2BcR4g4riY6ACe3kA): We met with Jason and he suggested these to us - but we'll probably go with an electromechanical one because we found one that matches our coil voltage and current.

For our ESP programmer circuit design, we looked at the [example](https://courses.engr.illinois.edu/ece445/wiki/#/esp32_example/index) on the 445 wiki. We based ours off that circuit, which provided a really good base circuit for us to use. We thought about removing the high voltage aspect part of the circuit and replacing it with a micro-USB. This way, we don't have to worry about the heat sink as well as it being potentially hazardous to work with high voltage. A micro-USB will succesfully plug into the outlet and convert the 120 V AC current to a 5 V DC current which we can then input into our linear regulator. 


# 10/16/2023

We are working on the PCB because we hope to get it in for the PCB order tomorrow. 

![](esp_programmer.png)

One thing we aren't 100% sure yet is some of our pin connections - one thing we had to look into was RX/TX. We initially thought RX -> RX and TX -> TX, but it seems that it should be RX -> RX. If we need any more changes to our PCB design, we'll revise it and submit another design for another PCB order. Another change we've thought of making is to add a flyback diode (to prevent sudden voltage spikes) and a transistor so that our current is flowing in the current direction. 


# 10/23/2023

I have started to work on the voice recognition of our project. We've decided to go with VOSK API - I've been reading documentation and watching a few tutorials to see how we can set it up. One thing I really like about Vosk is that there's a few different models for us to choose from - so I'll experiment a bit and see if there are any that work for us. 

# 10/30/2023

We've placed in orders for our parts, trying to use ones that we found that matches the footprints on the KiCad. This was a bit difficult as we weren't exactly sure about some of them like the switches. We have also been coordinating with the machine shop to build any enclosures we need for our project. 

![](lamp_box.png)

This is kind of what we envision our end product for the lamp box to look like. 

# 11/11/2023

We have received our PCB orders and most of our parts and have begun to take the final steps into finishing our project. While my teammates are working on the soldering, I am finishing up on writing the software that will be loaded onto the Raspberry Pi. I also took a bit of time to set up the Raspberry Pi - the configurations are kinda weird (Raspberry Pi's default keyboard is the UK one which made coding very annoying). In the end, I was able to test our voice recognition and successfully get it to recognize sounds. The accuracy doesn't seem to be exactly what we want it to be, but I can fine tune it a bit after. Right now, with 10 phrases spoken, it'll accurately determine it about 6 times, which does not meet our requirements. I'll do a bit of research and look into potential algorithms - one thing I was looking at was looking at the sequence to determine the order of other words, but I'll jot that down in my next entry. 

# 11/14/2023

I've been working on a few ways to improve the accuracy of our program. One thing I'm thinking about is detecting phrases if a word isn't spoken as correctly. For example, if the word "outwit" is detected, we can look at the other sequence of words. For example, if the other words "one on" was detected, we can assume that the phrase should have been "outlet one on." With this, it seemed to be able to improve our accuracy to 72%, which is good for us! I'll run some tests later and not them in the next few entries. We almost have everything done - we have soldered our PCBs, and while it may not work, we have come up with a backup solution to test with the dev board instead. We have used a multimeter to detect the voltages of our circuit - 5 V in, 3.3 V out which is correct. So if our PCB doesn't work, it could be due to a soldering issue or maybe we shorted something. It still doesn't seem like we can program our ESP - we do want to test the relay to see if it is working correctly, but we seem unable to find a way to supply a 5V directly into the circuit. Looking back on our soldering, however, it also could seem like we could've been a lot more careful - there are a few burn marks across the PCB, and some of the pins do seem to be shorted on our microcontroller. We're looking into ordering more parts, but given that it took a bit over two weeks for our last parts to arrive, we're not sure if we will get it in time, so we may have to go with the backup option. 

![](linear_regulator.png)

This goes with what we had designed with our linear regulator - so that part of the circuit is correct. However, we still aren't able to debug why we aren't able to program our ESP, so we will have to look at some other approaches. 

# 11/27/2023

We are getting ready for our demo. We are going to try to verify as many of our high level requirements as possible. 

High Level Requirements: 

Our system is able to recognize 6 commands with 75% accuracy from three different voices that are facing the microphone up to 1 meter from the main station box. This was a success. We were able to recognize "Outlet one on", "outlet one off", "outlet two on", "outlet two off", "all outlets on", and "all outlets off," while standing 1.5 meters away from the microphone. We tested the accuracy by having three different people say each command thrice, and 47/54 commands were successfully recognized, demonstrating an accuracy of 87%.

Our system supports up to two independent lamp boxes that can communicate with the main station box up to 5 meters away. This was half a sucess - our PCB's didn't work, so we tested on a dev board instead. We only had one devboard, so we were only able to get one lamp box working.

There will be visual feedback from the lamp boxes is displayed within 5 seconds of the microphone picking up a command. This was also a success. We determined that there was a visual change within 3 seconds when speaking a command.

I'm going to focus on my verifications for the voice recognition subsystem. 

![](voice_test.png)

# 12/5/2023

Final thoughts: 

Going to spend some time working on the final paper. I would like to add all of our verification data to show which requirements we have fulfilled. 


